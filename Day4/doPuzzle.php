<?php

$puzzleInput = 'bgvyzdsv';

$traversingNum = 1;

$hexNumber = "";

$has5LeadingZeroes = false;
$i = 0;
$found = false;
$time_pre = microtime(true);
while(!$found){
	echo 'Checking ' . $puzzleInput.$i . "\n";
	$control = md5($puzzleInput.$i);
	echo 'Checking ' . $control . "\n";
	if(strlen($control) >= 6){
		echo substr($control,0,6) . "\n";
		if(substr($control, 0, 6) === "000000"){
			echo "\nFound smallest number: " . $i . "\n";
			$found = true;
			$time_post = microtime(true);
			$time_taken = $time_post - $time_pre;
			echo "\n\nTime taken: " . $time_taken;
		}
	}
	$i++;
}

?>